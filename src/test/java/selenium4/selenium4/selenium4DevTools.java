package selenium4.selenium4;

import io.github.bonigarcia.wdm.WebDriverManager;

import static org.junit.Assert.assertEquals;

import java.util.Optional;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.devtools.DevTools;
import org.openqa.selenium.devtools.log.Log;
import org.openqa.selenium.devtools.network.Network;
import org.openqa.selenium.devtools.network.model.ConnectionType;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;


import org.slf4j.*;


public class selenium4DevTools {
	
	final static Logger logger = LoggerFactory.getLogger(selenium4DevTools.class);
	
	private static ChromeDriver driver;
	private static DevTools chromeDevTools;
	
	@Before
	public void setUp() {
		
		WebDriverManager.chromedriver().setup();
		
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--incognito");
		

		driver = new ChromeDriver(options);
		
		driver.get("https://coffeestainio.github.io/");
		driver.manage().window().maximize();

		chromeDevTools = driver.getDevTools();
		
		chromeDevTools.createSession();
		
	}
	
	@After
	public void TearDown() {
		driver.close();
		driver.quit();
	}
	
	//@Test
	public void enableNetworkOffline() throws InterruptedException  {
		

		
		chromeDevTools.send(Network.enable(Optional.of(100), Optional.empty(), Optional.empty()));
		
		chromeDevTools.send(Network.emulateNetworkConditions(true,100,1000,2000,Optional.of(ConnectionType.WIFI)));
		
		chromeDevTools.addListener(Network.loadingFailed(), loadingFailed -> assertEquals(loadingFailed.getErrorText(),"net::ERR_INTERNET_DISCONNECTED\""));
		
		
		
		 driver.get("urlSite");
		 
		 Thread.sleep(100000);
	}
	
	//@Test
	public void consoleLogsTest() {
		chromeDevTools.send(Log.enable());
		
		chromeDevTools.addListener(Log.entryAdded(), entry -> System.out.println(entry.getText()));
		driver.get("https://coffeestainio.github.io/");
	}

}
