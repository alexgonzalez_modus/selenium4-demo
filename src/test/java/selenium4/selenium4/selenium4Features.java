package selenium4.selenium4;

import io.github.bonigarcia.wdm.WebDriverManager;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WindowType;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.locators.RelativeLocator;
import org.junit.Before;
import org.junit.After;

public class selenium4Features {
	
	WebDriver driver;
	
	@Before
	public void setUp() {

		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		
		driver.get("https://coffeestainio.github.io/");
		driver.manage().window().maximize();
	}
	
	@After
	public void TearDown() {
		driver.close();
		driver.quit();
	}
	

	
	// @Test
	public void openNewTab() throws InterruptedException {
		driver.switchTo().newWindow(WindowType.TAB);
		driver.navigate().to("http://www.google.com");
		
		Thread.sleep(5000);
		
	}
	
	// @Test
	public void openNewWindow() throws InterruptedException {
		driver.switchTo().newWindow(WindowType.WINDOW);
		driver.navigate().to("http://www.google.com");
		
		Thread.sleep(5000);
		
	}
	 
	// @Test
	 public void getElementLocation() {
		 WebElement banner = driver.findElement(By.xpath("//section"));
		 
		 System.out.println("Height: " + banner.getRect().getDimension().getHeight());
		 System.out.println("Width: " + banner.getRect().getDimension().getWidth());
		 
		 System.out.println("X Location: " + banner.getRect().getX());
		 System.out.println("Y Location: " + banner.getRect().getY());
		 
	 }
	 
	// @Test
	 public void relativeLocatorsTest() throws InterruptedException {
		 WebElement paragraph = driver.findElement(RelativeLocator.withTagName("p")
				 .below(By.className("img-fluid"))
				 .above(By.xpath("//a[contains(.,'Follow Us')]")));
		 Thread.sleep(5000);
		 System.out.println("Text:" + paragraph.getText() );
		 
	 }
	 

	 
	

}
