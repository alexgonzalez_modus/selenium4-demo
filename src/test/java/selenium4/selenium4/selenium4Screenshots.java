package selenium4.selenium4;

import io.github.bonigarcia.wdm.WebDriverManager;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.locators.RelativeLocator;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;


public class selenium4Screenshots {
	
	WebDriver driver;
	
	@Before
	public void setUp() {

		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		
		driver.get("https://coffeestainio.github.io/");
		driver.manage().window().maximize();
	}
	
	@After
	public void TearDown() {
		driver.close();
		driver.quit();
	}
	
	@Test
	public void getFullScreenshot() throws IOException {

		WebElement paragraph = driver.findElement(RelativeLocator.withTagName("p")
				 .below(By.className("img-fluid"))
				 .above(By.xpath("//a[contains(.,'Follow Us')]")));
		
		highlightElement(paragraph,driver);
		
		TakesScreenshot ts = (TakesScreenshot)driver;
		
		File file = ts.getScreenshotAs(OutputType.FILE);
		
		File destinationFile = new File("fullScreenshot.png");
		
		FileUtils.copyFile(file, destinationFile);
	}
	
	// @Test
	public void getElementScreenshot() throws IOException {
		
		WebElement banner = driver.findElement(By.xpath("//section"));
		File file = banner.getScreenshotAs(OutputType.FILE);
		
		File destinationFile = new File("banner.png");
		FileUtils.copyFile(file, destinationFile);
		
	}
	
	 
	void highlightElement(WebElement ele, WebDriver driver) {
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].style.border='2px solid red'", ele);
	}
	 

}
